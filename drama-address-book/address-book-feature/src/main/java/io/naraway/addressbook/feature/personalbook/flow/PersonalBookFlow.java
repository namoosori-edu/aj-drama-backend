package io.naraway.addressbook.feature.personalbook.flow;

import io.naraway.accent.domain.type.NameValueList;
import io.naraway.addressbook.aggregate.address.domain.entity.AddressPage;
import io.naraway.addressbook.aggregate.address.domain.entity.sdo.AddressBookCdo;
import io.naraway.addressbook.aggregate.address.domain.entity.sdo.AddressPageCdo;
import io.naraway.addressbook.aggregate.address.domain.logic.AddressBookLogic;
import io.naraway.addressbook.aggregate.address.domain.logic.AddressPageLogic;
import io.naraway.addressbook.feature.personalbook.action.PersonalBookAction;
import io.naraway.addressbook.feature.shared.support.NoIdenticalTeamException;
import io.naraway.addressbook.feature.teambook.action.TeamBookAction;
import io.naraway.drama.prologue.spacekeeper.support.DramaRequestContext;
import io.naraway.drama.prologue.spacekeeper.support.NoIdenticalPersonException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Service
@Transactional
public class PersonalBookFlow {
    //
    private final AddressBookLogic addressBookLogic;
    private final AddressPageLogic addressPageLogic;
    private final PersonalBookAction personalBookAction;
    private final TeamBookAction teamBookAction;

    public PersonalBookFlow(AddressBookLogic addressBookLogic,
                            AddressPageLogic addressPageLogic,
                            PersonalBookAction personalBookAction,
                            TeamBookAction teamBookAction) {
        //
        this.addressBookLogic = addressBookLogic;
        this.addressPageLogic = addressPageLogic;
        this.personalBookAction = personalBookAction;
        this.teamBookAction = teamBookAction;
    }

    public String registerPersonalBook(AddressBookCdo addressBookCdo) {
        //
        String citizenId = addressBookCdo.getAddressBookId();           // Rule: AddressBook id should be a citizen id
        if (!personalBookAction.isIdenticalPerson(citizenId)) {
            throw new NoIdenticalPersonException(
                    DramaRequestContext.current().getCitizenId(),
                    citizenId
            );
        }

        String addressBookId = addressBookCdo.getAddressBookId();
        if (addressBookLogic.existsAddressBook(addressBookId)) {
            throw new DuplicateKeyException("AddressBook already exists: " + addressBookId);
        }

        return addressBookLogic.registerAddressBook(addressBookCdo);
    }

    public void modifyPersonalBook(String addressBookId, NameValueList nameValueList) {
        //
        String citizenId = addressBookId;
        if (!personalBookAction.isIdenticalPerson(citizenId)) {
            throw new IllegalArgumentException("No identical: requester and request info.");
        }

        addressBookLogic.modifyAddressBook(addressBookId, nameValueList);
    }

    public String registerPersonalPage(AddressPageCdo addressPageCdo) {
        //
        String citizenId = addressPageCdo.getAddressBookId();           // Rule: AddressBook id should be a citizen id
        if (!personalBookAction.isIdenticalPerson(citizenId)) {
            throw new NoIdenticalPersonException(
                    DramaRequestContext.current().getCitizenId(),
                    citizenId
            );
        }

        return addressPageLogic.registerAddressPage(addressPageCdo);
    }

    public void modifyPersonalPage(String addressPageId, NameValueList nameValueList) {
        //
        AddressPage addressPage = addressPageLogic.findAddressPage(addressPageId);
        if (addressPage != null) {
            if (!personalBookAction.isIdenticalPerson(addressPage.getAddressBookId())) {
                throw new IllegalArgumentException("No identical: requester and request info.");
            }
            addressPageLogic.modifyAddressPage(addressPageId, nameValueList);
        } else {
            throw new NoSuchElementException("AddressPage id: " + addressPageId);
        }
    }

    public void assignPersonalBaseAddress(String addressBookId, String addressPageId) {
        //
        String citizenId = addressBookId;
        if (!personalBookAction.isIdenticalPerson(citizenId)) {
            throw new IllegalArgumentException("No identical: requester and request info.");
        }

        AddressPage originAddressPage = addressPageLogic.findByAddressBookIdAndBaseAddress(addressBookId, true);
        if (originAddressPage != null) {
            originAddressPage.setBaseAddress(false);
            addressPageLogic.modifyAddressPage(originAddressPage);
        }
        AddressPage targetAddressPage = addressPageLogic.findAddressPage(addressPageId);
        if (targetAddressPage == null) {
            throw new NoSuchElementException("AddressPage id: " + addressPageId);
        }
        targetAddressPage.setBaseAddress(true);
        addressPageLogic.modifyAddressPage(targetAddressPage);
    }

    public void migratePersonalPage(String addressPageId, String teamAddressBookId) {
        AddressPage addressPage = addressPageLogic.findAddressPage(addressPageId);
        if (addressPage == null) {
            throw new NoSuchElementException("PersonalAddressPage id: " + addressPageId);
        }
        if (!personalBookAction.isIdenticalPerson(addressPage.getAddressBookId())) {
            throw new IllegalArgumentException("No identical: requester and request info.");
        }

        if (!addressBookLogic.existsAddressBook(teamAddressBookId)) {
            throw new NoSuchElementException("TeamAddressBook id: " + addressPageId);
        }
        if (!teamBookAction.isIdenticalTeam(teamAddressBookId)) {
            throw new NoIdenticalTeamException(
                    DramaRequestContext.current().getCineroomIds(),
                    teamAddressBookId
            );
        }

        addressPage.setAddressBookId(teamAddressBookId);
        addressPage.setBaseAddress(false);
        addressPageLogic.modifyAddressPage(addressPage);
    }
}
