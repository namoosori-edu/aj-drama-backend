package io.naraway.addressbook.feature.personalbook.action;

import io.naraway.drama.prologue.spacekeeper.support.DramaRequestContext;
import org.springframework.stereotype.Component;

@Component
public class PersonalBookAction {
    //
    public boolean isIdenticalPerson(String citizenId) {
        //
        if (citizenId.equals(DramaRequestContext.current().getCitizenId())) {
            return true;
        }
        return false;
    }

    public boolean hasRole(String role) {
        //
        if (DramaRequestContext.current().hasRole(role)) {
            return true;
        }
        return false;
    }
}
