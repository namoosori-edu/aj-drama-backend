/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.addressbook.aggregate.address.domain.entity.sdo;

import io.naraway.addressbook.aggregate.address.domain.entity.vo.Address;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import io.naraway.drama.prologue.domain.ddd.CreationDataObject;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.drama.prologue.spacekeeper.support.DramaRequestContext;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressPageCdo extends CreationDataObject {
    //
    private String name;
    private Address address;
    private String phoneNumber;
    private String addressBookId;

    @Override
    public String genId() {
        /* Autogen by nara studio */
        return super.genId();
    }

    @Override
    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static AddressPageCdo fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, AddressPageCdo.class);
    }

    public static AddressPageCdo sample() {
        //
        DramaRequestContext.setSampleContext();
        return new AddressPageCdo(
                "Home",
                Address.sampleKorean(),
                "+82 10-9999-9999",
                AddressBookCdo.sample().genId()
        );
    }

    public static AddressPageCdo teamPageSample() {
        //
        DramaRequestContext.setSampleContext();
        return new AddressPageCdo(
                "Namoosori",
                Address.sampleKorean(),
                "+82 10-8888-8888",
                AddressBookCdo.teamBookSample().genId()
        );
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sample().toPrettyJson());
    }
}
