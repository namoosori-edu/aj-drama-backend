/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.addressbook.aggregate.address.domain.entity.sdo;

import io.naraway.accent.domain.key.tenant.CineroomKey;
import io.naraway.accent.domain.key.tenant.CitizenKey;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import io.naraway.drama.prologue.domain.ddd.CreationDataObject;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.drama.prologue.spacekeeper.support.DramaRequestContext;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressBookCdo extends CreationDataObject {
    //
    private String name;
    private String description;

    private String addressBookId;

    @Override
    public String genId() {
        //
        return addressBookId; // Decision is made in Feature layer
    }

    @Override
    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static AddressBookCdo fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, AddressBookCdo.class);
    }

    public static AddressBookCdo sample() {
        /* Autogen by nara studio */
        DramaRequestContext.setSampleContext();
        return new AddressBookCdo(
                "Terry's AddressPage Book",
                "Terry's Shipping List",
                CitizenKey.sample().getId()
        );
    }
    public static AddressBookCdo teamBookSample() {
        /* Autogen by nara studio */
        DramaRequestContext.setSampleContext();
        return new AddressBookCdo(
                "Nextree AddressPage Book",
                "Nextree Team Address List",
                CineroomKey.sample().getId()
        );
    }


    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sample().toPrettyJson());
    }
}
