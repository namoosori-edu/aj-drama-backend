/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.addressbook.aggregate.address.store.mongo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import io.naraway.addressbook.aggregate.address.store.mongo.odm.AddressPageDoc;
import java.util.Optional;
import io.naraway.accent.domain.type.Offset;
import java.util.List;

public interface AddressPageMongoRepository extends MongoRepository<AddressPageDoc, String> {
    /* Autogen by nara studio */
    Page<AddressPageDoc> findByAddressBookId(String addressBookId, Pageable pageable);
    List<AddressPageDoc> findByAddressBookId(String addressBookId);
    Optional<AddressPageDoc> findByAddressBookIdAndBaseAddress(String addressBookId, boolean baseAddress);
}
