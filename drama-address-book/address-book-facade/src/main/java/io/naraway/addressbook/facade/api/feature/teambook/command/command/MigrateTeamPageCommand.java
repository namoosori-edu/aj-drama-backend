package io.naraway.addressbook.facade.api.feature.teambook.command.command;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.key.tenant.CineroomKey;
import io.naraway.accent.domain.trail.CommandRequest;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.addressbook.aggregate.address.domain.entity.sdo.AddressPageCdo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.Assert;

import java.util.UUID;

@AuthorizedRole(isPublic = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MigrateTeamPageCommand extends CommandRequest {
    //
    private String addressPageId;
    private String personalAddressBookId;

    public void validate() {
        //
        Assert.hasText(addressPageId, "addressPageId is required.");
        Assert.hasText(personalAddressBookId, "personalAddressBookId is required.");
    }

    public static MigrateTeamPageCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, MigrateTeamPageCommand.class);
    }

    public static MigrateTeamPageCommand sample() {
        //
        return new MigrateTeamPageCommand(
                AddressPageCdo.sample().genId(),
                CineroomKey.sample().getId()
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
