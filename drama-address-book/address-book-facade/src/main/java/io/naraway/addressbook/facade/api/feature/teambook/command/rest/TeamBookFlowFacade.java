package io.naraway.addressbook.facade.api.feature.teambook.command.rest;

import io.naraway.accent.domain.trail.CommandResponse;
import io.naraway.addressbook.facade.api.feature.teambook.command.command.*;

public interface TeamBookFlowFacade {
    CommandResponse registerTeamBook(RegisterTeamBookCommand command);
    CommandResponse modifyTeamBook(ModifyTeamBookCommand command);
    CommandResponse registerTeamPage(RegisterTeamPageCommand command);
    CommandResponse modifyTeamPage(ModifyTeamPageCommand command);
    CommandResponse assignTeamBaseAddress(AssignTeamBaseAddressCommand command);
    CommandResponse migratePersonalPage(MigrateTeamPageCommand command);
}
