/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.addressbook.facade.api.feature.teambook.command.command;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.key.tenant.CitizenKey;
import io.naraway.accent.domain.trail.CommandRequest;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.addressbook.aggregate.address.domain.entity.sdo.AddressPageCdo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.Assert;

import java.util.UUID;


@AuthorizedRole(isPublic = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AssignTeamBaseAddressCommand extends CommandRequest {
    //
    private String addressBookId;
    private String addressPageId;

    public void validate() {
        //
        Assert.hasText(addressBookId, "addressBookId is required.");
        Assert.hasText(addressPageId, "addressPageId is required.");

    }

    public String toString() {
        //
        return toJson();
    }

    public static AssignTeamBaseAddressCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, AssignTeamBaseAddressCommand.class);
    }

    public static AssignTeamBaseAddressCommand sample() {
        //
        return new AssignTeamBaseAddressCommand(
                CitizenKey.sample().getId(),
                AddressPageCdo.sample().genId()
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}