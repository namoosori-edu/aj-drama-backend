import React, { FC } from 'react';
import { observer, useLocalObservable } from 'mobx-react';
import { Box, Button, Grid, Pagination, Paper, Typography } from '@mui/material';
import AddressPageItemView from './view/AddressPageItemView';
import { AddressPage } from '../../../../../api';
import { Add, ReportSharp } from '@mui/icons-material';

interface Props {
  onClick?: (addressPage: AddressPage) => void;
  onClickRegister?: () => void;
}

const ContentContainer: FC<Props> = observer(
  ({
    onClick = () => {},
    onClickRegister = () => {},
  }) => {
    //

    return addressPages.length ? (
      <>
        <Grid container spacing={5}>
          {addressPages.map(addressPage => (
            <Grid item xs={3} key={addressPage.id}>
              <AddressPageItemView
                addressPage={addressPage}
                onClickDefault={() => onClickDefault(addressPage)}
                onClick={() => onClick(addressPage)}
              />
            </Grid>
          ))}
        </Grid>
        <Box sx={{ width: '100%', mt: 5, display: 'flex', justifyContent: 'center' }}>
          <Pagination
            color="primary"
            shape="rounded"
            count={Math.ceil(totalCount / offset.limit)}
            onChange={onChangePage}
          />
        </Box>
      </>
    ) : (
      <Paper sx={{ p: 10, textAlign: 'center' }}>
        <ReportSharp sx={{ fontSize: 120, color: 'lightgrey' }} />
        <Box>
          <Typography color="lightgrey" variant="subtitle1">"등록된 연락처가 없습니다."</Typography>
          <Typography display="inline" fontWeight="bold" color="grey" variant="subtitle1">연락처 등록하기</Typography>
          <Typography display="inline" color="lightgrey" variant="subtitle1">를 클릭하여 연락처를 등록하시기 바랍니다.</Typography>
        </Box>
        <Button variant="contained" color="secondary" sx={{ my: 5 }} startIcon={<Add />} onClick={onClickRegister}>연락처 등록하기</Button>
      </Paper>
    );
  });

export default ContentContainer;
