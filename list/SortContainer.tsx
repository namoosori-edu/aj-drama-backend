import React, { FC } from 'react';
import { observer, useLocalObservable } from 'mobx-react';
import { Box, Button, Divider } from '@mui/material';

interface Props {
}

const SortContainer: FC<Props> = observer(
  ({
  }) => {
    //

    return (
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          width: 'fit-content',
          color: 'text.secondary',
        }}
      >
        <Button
          color="inherit"
          onClick={() => onChangeSortingField('registrationTime')}
        >
          등록일순
        </Button>
        <Divider orientation="vertical" variant="middle" flexItem />
        <Button
          color="inherit"
          onClick={() => onChangeSortingField('name')}
        >
          이름순
        </Button>
      </Box>
    );
  });

export default SortContainer;
