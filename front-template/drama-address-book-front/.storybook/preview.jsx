import * as React from "react";
import {configure, isObservableArray} from 'mobx';
import {AppContext, dialogUtil, axiosApi} from "@nara/prologue";
import {ThemeProvider} from "@mui/material";
import {default as theme} from './config/theme';
import {default as DialogView} from './config/dialog';


configure({
  useProxies: 'ifavailable',
  isolateGlobalState: true,
  computedRequiresReaction: true,
  reactionRequiresObservable: true,
});

(() => {
  const isArray = Array.isArray;
  Object.defineProperty(Array, 'isArray', {
    value: (target) => (isObservableArray(target) || isArray(target)),
  });
})();

export const parameters = {
  actions: {argTypesRegex: '^on[A-Z].*'},
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

axiosApi.interceptors.request.use((request) => {
  request.headers.actorId = `1@1:1:1:1-1`;
  request.headers.roles = `user, director`;
  return request;
});

export const decorators = [
  Story => {
    //
    return (
      <div>
        <AppContext.Provider>
          <ThemeProvider theme={theme}>
            <dialogUtil.Viewer renderDialog={(params) => (<DialogView {...params} />)}/>
                <Story/>
          </ThemeProvider>
        </AppContext.Provider>
      </div>
    );
  },
];
