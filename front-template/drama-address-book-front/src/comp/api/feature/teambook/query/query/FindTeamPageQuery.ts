import { QueryRequest } from '@nara/accent';
import { AddressPage } from '~/comp';


class FindTeamPageQuery extends QueryRequest<AddressPage> {
  addressPageId: string;
  
  constructor(addressPageId: string) {
    super(AddressPage);
    this.addressPageId = addressPageId;
  }

  static fromDomain(domain: FindTeamPageQuery): FindTeamPageQuery {
    const query = new FindTeamPageQuery(
      domain.addressPageId,
    );
    
    query.setResponse(domain);
    return query;
  }

  static byQuery(addressPageId: string) {
    const query = new FindTeamPageQuery(
      addressPageId,
    );
    
    return query;
  }

}

export default FindTeamPageQuery;

