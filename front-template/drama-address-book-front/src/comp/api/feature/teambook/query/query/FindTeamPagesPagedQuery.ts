import { QueryRequest } from '@nara/accent';
import { AddressPage } from '~/comp';


class FindTeamPagesPagedQuery extends QueryRequest<AddressPage> {
  addressBookId: string;
  
  constructor(addressBookId: string) {
    super(AddressPage);
    this.addressBookId = addressBookId;
  }

  static fromDomain(domain: FindTeamPagesPagedQuery): FindTeamPagesPagedQuery {
    const query = new FindTeamPagesPagedQuery(
      domain.addressBookId,
    );
    
    query.setResponse(domain);
    return query;
  }

  static byQuery(addressBookId: string) {
    const query = new FindTeamPagesPagedQuery(
      addressBookId,
    );
    
    return query;
  }

}

export default FindTeamPagesPagedQuery;

