import { ApiClient } from '@nara/prologue';
import { AddressBook, AddressPage } from '~/comp';
import {
  FindTeamBookQuery,
  FindTeamPagesQuery,
  FindTeamPagesPagedQuery,
  FindTeamPageQuery,
} from '../query';


class TeamBookSeekQueryApiStub {
  static _instance: TeamBookSeekQueryApiStub;
  
  private readonly client = new ApiClient('/api/addressbook/feature/team-book-seek/query', {
    resDataName: 'queryResult',
  });
  
  static get instance() {
    if (!TeamBookSeekQueryApiStub._instance) {
      TeamBookSeekQueryApiStub._instance = new TeamBookSeekQueryApiStub();
    }
    return TeamBookSeekQueryApiStub._instance;
  }

  async findTeamBook(query: FindTeamBookQuery): Promise<AddressBook> {
    return this.client.postNotNull<AddressBook>(
      AddressBook,
      '/find-team-book',
      query
    );
  }

  async findTeamPages(query: FindTeamPagesQuery): Promise<AddressPage[]> {
    return this.client.postArray<AddressPage>(
      AddressPage,
      '/find-team-pages',
      query
    );
  }

  async findTeamPagesPaged(query: FindTeamPagesPagedQuery): Promise<AddressPage[]> {
    return this.client.postArray<AddressPage>(
      AddressPage,
      '/find-team-pages-paged',
      query
    );
  }

  async findTeamPage(query: FindTeamPageQuery): Promise<AddressPage> {
    return this.client.postNotNull<AddressPage>(
      AddressPage,
      '/find-team-page',
      query
    );
  }

}

export default TeamBookSeekQueryApiStub;

