import { CommandRequest, CommandType } from '@nara/accent';


class MigrateTeamPageCommand extends CommandRequest {
  addressPageId: string;
  personalAddressBookId: string;
  
  constructor(addressPageId: string, personalAddressBookId: string) {
    super(CommandType.UserDefined);
    this.addressPageId = addressPageId;
    this.personalAddressBookId = personalAddressBookId;
  }

  static new(addressPageId: string, personalAddressBookId: string) {
    const command = new MigrateTeamPageCommand(
      addressPageId,
      personalAddressBookId,
    );
    
    return command;
  }

}

export default MigrateTeamPageCommand;

