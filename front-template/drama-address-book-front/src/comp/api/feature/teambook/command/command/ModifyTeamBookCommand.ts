import { CommandRequest, NameValueList, CommandType } from '@nara/accent';


class ModifyTeamBookCommand extends CommandRequest {
  addressBookId: string;
  nameValueList: NameValueList;
  
  constructor(addressBookId: string, nameValueList: NameValueList) {
    super(CommandType.UserDefined);
    this.addressBookId = addressBookId;
    this.nameValueList = nameValueList;
  }

  static new(addressBookId: string, nameValueList: NameValueList) {
    const command = new ModifyTeamBookCommand(
      addressBookId,
      nameValueList,
    );
    
    return command;
  }

}

export default ModifyTeamBookCommand;

