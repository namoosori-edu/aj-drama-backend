import { CommandResponse, NameValueList } from '@nara/accent';
import { ApiClient } from '@nara/prologue';
import { AddressBookCdo, AddressPageCdo } from '~/comp';
import {
  RegisterTeamBookCommand,
  ModifyTeamBookCommand,
  RegisterTeamPageCommand,
  ModifyTeamPageCommand,
  AssignTeamBaseAddressCommand,
  MigrateTeamPageCommand,
} from '../command';


class TeamBookFlowApiStub {
  static _instance: TeamBookFlowApiStub;
  
  private readonly client = new ApiClient('/api/addressbook/teambook');
  
  static get instance() {
    if (!TeamBookFlowApiStub._instance) {
      TeamBookFlowApiStub._instance = new TeamBookFlowApiStub();
    }
    return TeamBookFlowApiStub._instance;
  }

  async registerTeamBook(addressBookCdo: AddressBookCdo): Promise<CommandResponse> {
    const command = RegisterTeamBookCommand.new(
      addressBookCdo,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/register-team-book', command);
  }

  async modifyTeamBook(addressBookId: string, nameValueList: NameValueList): Promise<CommandResponse> {
    const command = ModifyTeamBookCommand.new(
      addressBookId,
      nameValueList,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/modify-team-book', command);
  }

  async registerTeamPage(addressPageCdo: AddressPageCdo): Promise<CommandResponse> {
    const command = RegisterTeamPageCommand.new(
      addressPageCdo,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/register-team-page', command);
  }

  async modifyTeamPage(addressPageId: string, nameValueList: NameValueList): Promise<CommandResponse> {
    const command = ModifyTeamPageCommand.new(
      addressPageId,
      nameValueList,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/modify-team-page', command);
  }

  async assignTeamBaseAddress(addressBookId: string, addressPageId: string): Promise<CommandResponse> {
    const command = AssignTeamBaseAddressCommand.new(
      addressBookId,
      addressPageId,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/assign-team-base-address', command);
  }

  async migratePersonalPage(addressPageId: string, personalAddressBookId: string): Promise<CommandResponse> {
    const command = MigrateTeamPageCommand.new(
      addressPageId,
      personalAddressBookId,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/migrate-personal-page', command);
  }

}

export default TeamBookFlowApiStub;

