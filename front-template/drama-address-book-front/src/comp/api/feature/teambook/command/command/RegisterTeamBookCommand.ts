import { CommandRequest, CommandType } from '@nara/accent';
import { AddressBookCdo } from '~/comp';


class RegisterTeamBookCommand extends CommandRequest {
  addressBookCdo: AddressBookCdo;
  
  constructor(addressBookCdo: AddressBookCdo) {
    super(CommandType.UserDefined);
    this.addressBookCdo = addressBookCdo;
  }

  static new(addressBookCdo: AddressBookCdo) {
    const command = new RegisterTeamBookCommand(
      addressBookCdo,
    );
    
    return command;
  }

}

export default RegisterTeamBookCommand;

