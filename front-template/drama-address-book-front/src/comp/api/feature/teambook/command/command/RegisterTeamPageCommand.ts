import { CommandRequest, CommandType } from '@nara/accent';
import { AddressPageCdo } from '~/comp';


class RegisterTeamPageCommand extends CommandRequest {
  addressPageCdo: AddressPageCdo;
  
  constructor(addressPageCdo: AddressPageCdo) {
    super(CommandType.UserDefined);
    this.addressPageCdo = addressPageCdo;
  }

  static new(addressPageCdo: AddressPageCdo) {
    const command = new RegisterTeamPageCommand(
      addressPageCdo,
    );
    
    return command;
  }

}

export default RegisterTeamPageCommand;

