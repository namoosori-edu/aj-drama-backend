import { QueryRequest } from '@nara/accent';
import { AddressPage } from '~/comp';


class FindPersonalPagesPagedQuery extends QueryRequest<AddressPage> {
  addressBookId: string;
  
  constructor(addressBookId: string) {
    super(AddressPage);
    this.addressBookId = addressBookId;
  }

  static fromDomain(domain: FindPersonalPagesPagedQuery): FindPersonalPagesPagedQuery {
    const query = new FindPersonalPagesPagedQuery(
      domain.addressBookId,
    );
    
    query.setResponse(domain);
    return query;
  }

  static byQuery(addressBookId: string) {
    const query = new FindPersonalPagesPagedQuery(
      addressBookId,
    );
    
    return query;
  }

}

export default FindPersonalPagesPagedQuery;

