import { ApiClient } from '@nara/prologue';
import { AddressBook, AddressPage } from '~/comp';
import {
  FindPersonalBookQuery,
  FindPersonalPagesQuery,
  FindPersonalPagesPagedQuery,
  FindPersonalPageQuery,
} from '../query';


class PersonalBookSeekQueryApiStub {
  static _instance: PersonalBookSeekQueryApiStub;
  
  private readonly client = new ApiClient('/api/addressbook/feature/personal-book-seek/query', {
    resDataName: 'queryResult',
  });
  
  static get instance() {
    if (!PersonalBookSeekQueryApiStub._instance) {
      PersonalBookSeekQueryApiStub._instance = new PersonalBookSeekQueryApiStub();
    }
    return PersonalBookSeekQueryApiStub._instance;
  }

  async findPersonalBook(query: FindPersonalBookQuery): Promise<AddressBook> {
    return this.client.postNotNull<AddressBook>(
      AddressBook,
      '/find-personal-book',
      query
    );
  }

  async findPersonalPages(query: FindPersonalPagesQuery): Promise<AddressPage[]> {
    return this.client.postArray<AddressPage>(
      AddressPage,
      '/find-personal-pages',
      query
    );
  }

  async findPersonalPagesPaged(query: FindPersonalPagesPagedQuery): Promise<AddressPage[]> {
    return this.client.postArray<AddressPage>(
      AddressPage,
      '/find-personal-pages-paged',
      query
    );
  }

  async findPersonalPage(query: FindPersonalPageQuery): Promise<AddressPage> {
    return this.client.postNotNull<AddressPage>(
      AddressPage,
      '/find-personal-page',
      query
    );
  }

}

export default PersonalBookSeekQueryApiStub;

