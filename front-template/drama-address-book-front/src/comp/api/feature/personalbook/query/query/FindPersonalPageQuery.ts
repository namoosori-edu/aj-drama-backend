import { QueryRequest } from '@nara/accent';
import { AddressPage } from '~/comp';


class FindPersonalPageQuery extends QueryRequest<AddressPage> {
  addressPageId: string;
  
  constructor(addressPageId: string) {
    super(AddressPage);
    this.addressPageId = addressPageId;
  }

  static fromDomain(domain: FindPersonalPageQuery): FindPersonalPageQuery {
    const query = new FindPersonalPageQuery(
      domain.addressPageId,
    );
    
    query.setResponse(domain);
    return query;
  }

  static byQuery(addressPageId: string) {
    const query = new FindPersonalPageQuery(
      addressPageId,
    );
    
    return query;
  }

}

export default FindPersonalPageQuery;

