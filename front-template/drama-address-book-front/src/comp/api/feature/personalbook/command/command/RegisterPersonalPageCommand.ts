import { CommandRequest, CommandType } from '@nara/accent';
import { AddressPageCdo } from '~/comp';


class RegisterPersonalPageCommand extends CommandRequest {
  addressPageCdo: AddressPageCdo;
  
  constructor(addressPageCdo: AddressPageCdo) {
    super(CommandType.UserDefined);
    this.addressPageCdo = addressPageCdo;
  }

  static new(addressPageCdo: AddressPageCdo) {
    const command = new RegisterPersonalPageCommand(
      addressPageCdo,
    );
    
    return command;
  }

}

export default RegisterPersonalPageCommand;

