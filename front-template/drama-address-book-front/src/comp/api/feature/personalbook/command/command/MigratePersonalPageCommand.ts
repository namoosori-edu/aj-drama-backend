import { CommandRequest, CommandType } from '@nara/accent';


class MigratePersonalPageCommand extends CommandRequest {
  addressPageId: string;
  teamAddressBookId: string;
  
  constructor(addressPageId: string, teamAddressBookId: string) {
    super(CommandType.UserDefined);
    this.addressPageId = addressPageId;
    this.teamAddressBookId = teamAddressBookId;
  }

  static new(addressPageId: string, teamAddressBookId: string) {
    const command = new MigratePersonalPageCommand(
      addressPageId,
      teamAddressBookId,
    );
    
    return command;
  }

}

export default MigratePersonalPageCommand;

