import { CommandRequest, NameValueList, CommandType } from '@nara/accent';


class ModifyPersonalBookCommand extends CommandRequest {
  addressBookId: string;
  nameValueList: NameValueList;
  
  constructor(addressBookId: string, nameValueList: NameValueList) {
    super(CommandType.UserDefined);
    this.addressBookId = addressBookId;
    this.nameValueList = nameValueList;
  }

  static new(addressBookId: string, nameValueList: NameValueList) {
    const command = new ModifyPersonalBookCommand(
      addressBookId,
      nameValueList,
    );
    
    return command;
  }

}

export default ModifyPersonalBookCommand;

