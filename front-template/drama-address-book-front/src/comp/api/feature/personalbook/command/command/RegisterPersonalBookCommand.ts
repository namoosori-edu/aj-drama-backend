import { CommandRequest, CommandType } from '@nara/accent';
import { AddressBookCdo } from '~/comp';


class RegisterPersonalBookCommand extends CommandRequest {
  addressBookCdo: AddressBookCdo;
  
  constructor(addressBookCdo: AddressBookCdo) {
    super(CommandType.UserDefined);
    this.addressBookCdo = addressBookCdo;
  }

  static new(addressBookCdo: AddressBookCdo) {
    const command = new RegisterPersonalBookCommand(
      addressBookCdo,
    );
    
    return command;
  }

}

export default RegisterPersonalBookCommand;

