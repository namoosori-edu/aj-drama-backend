import { CommandResponse, NameValueList } from '@nara/accent';
import { ApiClient } from '@nara/prologue';
import { AddressBookCdo, AddressPageCdo } from '~/comp';
import {
  RegisterPersonalBookCommand,
  ModifyPersonalBookCommand,
  RegisterPersonalPageCommand,
  ModifyPersonalPageCommand,
  AssignPersonalBaseAddressCommand,
  MigratePersonalPageCommand,
} from '../command';


class PersonalBookFlowApiStub {
  static _instance: PersonalBookFlowApiStub;
  
  private readonly client = new ApiClient('/api/addressbook/personalbook');
  
  static get instance() {
    if (!PersonalBookFlowApiStub._instance) {
      PersonalBookFlowApiStub._instance = new PersonalBookFlowApiStub();
    }
    return PersonalBookFlowApiStub._instance;
  }

  async registerPersonalBook(addressBookCdo: AddressBookCdo): Promise<CommandResponse> {
    const command = RegisterPersonalBookCommand.new(
      addressBookCdo,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/register-personal-book', command);
  }

  async modifyPersonalBook(addressBookId: string, nameValueList: NameValueList): Promise<CommandResponse> {
    const command = ModifyPersonalBookCommand.new(
      addressBookId,
      nameValueList,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/modify-personal-book', command);
  }

  async registerPersonalPage(addressPageCdo: AddressPageCdo): Promise<CommandResponse> {
    const command = RegisterPersonalPageCommand.new(
      addressPageCdo,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/register-personal-page', command);
  }

  async modifyPersonalPage(addressPageId: string, nameValueList: NameValueList): Promise<CommandResponse> {
    const command = ModifyPersonalPageCommand.new(
      addressPageId,
      nameValueList,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/modify-personal-page', command);
  }

  async assignPersonalBaseAddress(addressBookId: string, addressPageId: string): Promise<CommandResponse> {
    const command = AssignPersonalBaseAddressCommand.new(
      addressBookId,
      addressPageId,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/assign-personal-base-address', command);
  }

  async migratePersonalPage(addressPageId: string, teamAddressBookId: string): Promise<CommandResponse> {
    const command = MigratePersonalPageCommand.new(
      addressPageId,
      teamAddressBookId,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/migrate-personal-page', command);
  }

}

export default PersonalBookFlowApiStub;

