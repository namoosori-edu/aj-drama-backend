class AddressBookCdo {
  name: string;
  description: string;
  addressBookId: string;
  
  constructor(name: string, description: string, addressBookId: string) {
    this.name = name;
    this.description = description;
    this.addressBookId = addressBookId;
  }

  static new(): AddressBookCdo {
    return new AddressBookCdo('', '', '');
  }

}

export default AddressBookCdo;

