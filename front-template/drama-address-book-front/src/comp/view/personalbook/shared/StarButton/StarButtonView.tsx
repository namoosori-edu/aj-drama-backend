import React, { FC } from 'react';
import { observer } from 'mobx-react';
import { IconButton, IconButtonProps } from '@mui/material';
import { Star, StarBorder } from '@mui/icons-material';

interface Props extends IconButtonProps {
  baseAddress: boolean;
}

const StarButtonView: FC<Props> = observer(
  ({
    baseAddress,
    ...iconButtonProps
  }) => {
    return (
      <IconButton
        {...iconButtonProps}
        onClick={(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
          event.stopPropagation();
          if (iconButtonProps.onClick) {
            iconButtonProps.onClick(event);
          }
        }}
      >
        <Star style={{ color: baseAddress ? 'orange' : 'lightgrey' }} />
      </IconButton>
    );
  });

export default StarButtonView;
