
import React, { FC } from 'react';
import { observer } from 'mobx-react';
import { Box, Button } from '@mui/material';
import { makeStyles } from '@mui/styles';

interface Props {
  children: React.ReactNode;
}

const useStyles = makeStyles(() => ({
  action: {
    textAlign: 'center',
    marginTop: '30px',
    '& button': {
      marginRight: '10px',
    },
  },
}));

const ButtonGroupWrapperView: FC<Props> = observer(
  ({
    children,
  }) => {
    const classes = useStyles();

    return (
      <Box className={classes.action}>
        {children}
      </Box>
    );
  });

export default ButtonGroupWrapperView;
