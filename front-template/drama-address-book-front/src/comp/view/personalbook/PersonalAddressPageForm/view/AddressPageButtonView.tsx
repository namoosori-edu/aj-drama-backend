import React, { FC } from 'react';
import { observer } from 'mobx-react';
import { Button } from '@mui/material';
import { ButtonGroupWrapper } from '../../shared';

interface Props {
  onClickSave: () => void;
  onClickCancel: () => void;
}

const AddressPageButtonView: FC<Props> = observer(
  ({
    onClickSave,
    onClickCancel,
  }) => {
    return (
      <ButtonGroupWrapper>
        <Button size="large" variant="contained" onClick={onClickSave}>저장</Button>
        <Button size="large" variant="outlined" onClick={onClickCancel}>취소</Button>
      </ButtonGroupWrapper>
    );
  });

export default AddressPageButtonView;
